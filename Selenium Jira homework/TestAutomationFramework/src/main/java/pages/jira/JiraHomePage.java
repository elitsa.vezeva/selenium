package pages.jira;

import com.telerikacademy.testframework.Utils;
import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class JiraHomePage extends BaseJiraPage {

    public static final String STORY_SUMMARY = "Trello board  and card creation";
    public static final String STORY_DESCRIPION = "Description:\n" +
            "Create a board in Trello\n" +
            "\n" +
            "Narrative:\n" +
            "As a user to https://trello.com/ \n" +
            "I want to be able to create a new board with cards\n" +
            "So that I can manage some tasks in a project";
    public static final String BUG_SUMMARY = "Unable to create a board";
    public static final String BUG_DESCRIPTION = "When you try to create new board, the Create button is not active.\n" +
            "\n" +
            "Steps to reproduce:\n" +
            "1. Navigate to https://trello.com/ \n" +
            "2. Click Create button\n" +
            "\n" +
            "Expected result: \n" +
            "The Create drop down menu is visible.\n" +
            "\n" +
            "Actual result:\n" +
            "The Create drop down menu is not visible.";

    public JiraHomePage(WebDriver driver) {
        super(driver, "jira.homePage");
    }

    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

    public void login(String userKey) {
        String username = Utils.getConfigPropertyByKey("jira.user.username");
        String password = Utils.getConfigPropertyByKey("jira.user.password");
        navigateToPage();
        actions.waitForElementVisible("jira.login.username");
        actions.typeValueInField(username, "jira.login.username");
        actions.clickElement("jira.login.button");

        actions.waitFor(2000);

        actions.waitForElementVisible("jira.login.password");
        actions.typeValueInField(password, "jira.login.password");
        actions.clickElement("jira.login.button");
    }

    public void createStory() {

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@id='createGlobalItem']")));

        actions.clickElement("jira.create.button");
        actions.waitForElementVisible("jira.issue.type");
        actions.clickElement("jira.issue.type");
        actions.waitForElementVisible("jira.issue.type.story");
        actions.clickElement("jira.issue.type.story");
        actions.waitFor(2000);
        actions.typeValueInField(STORY_SUMMARY, "jira.summary");
        actions.typeValueInField(STORY_DESCRIPION, "jira.description");
//        actions.clickElement("jira.priority");
//        actions.waitForElementVisible("jira.priority.high");
//        actions.clickElement("jira.priority.high");
        actions.clickElement("jira.create.issue.button");

    }

    public void createBug() {

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@id='createGlobalItem']")));

        actions.clickElement("jira.create.button");
        actions.waitForElementVisible("jira.issue.type");
        actions.clickElement("jira.issue.type");
        actions.waitForElementVisible("jira.issue.type.bug");
        actions.clickElement("jira.issue.type.bug");
        actions.waitFor(2000);
        actions.typeValueInField(BUG_SUMMARY, "jira.summary");
        actions.typeValueInField(BUG_DESCRIPTION, "jira.description");
//        actions.clickElement("jira.priority");
//        actions.waitForElementVisible("jira.priority.high");
//        actions.clickElement("jira.priority.high");
        actions.clickElement("jira.create.issue.button");

    }

    public void linkIssues() {
        actions.waitFor(5000);
        actions.clickElement("jira.last.created.issue");
        actions.waitForElementVisible("jira.open.issue");
        actions.clickElement("jira.open.issue");
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ak-main-content']/div/div/div[2]/div[2]/div[2]/div[1]/div/div[1]/div/div[4]/span/div/button")));
//        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='css-77dq8j']")));
        actions.clickElement("jira.link.issue.button");

//        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("jira.link.issue.type.field")));
//        actions.waitForElementVisible("jira.link.issue.type.field");
//        actions.clickElement("jira.link.issue.type.field");
    }

}
