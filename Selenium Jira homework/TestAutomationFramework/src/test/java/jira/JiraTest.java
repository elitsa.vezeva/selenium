package jira;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.jira.JiraHomePage;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JiraTest extends BaseTest {

    @Test
    public void aLogin() {

        JiraHomePage home = new JiraHomePage(actions.getDriver());
        home.login("user");


    }

    @Test
    public void createAStory() {
        JiraHomePage home = new JiraHomePage(actions.getDriver());
        home.createStory();


    }

    @Test
    public void createBug() {
        JiraHomePage home = new JiraHomePage(actions.getDriver());
        home.createBug();


    }

//    @Test
//    public void linkStoryAndBug() {
//        JiraHomePage home = new JiraHomePage(actions.getDriver());
//        home.linkIssues();
//    }
}
