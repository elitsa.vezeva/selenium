import org.asynchttpclient.util.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class BingTest {

    @Test
    public void navigateToBing() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\user\\Drivers\\chromedriver.exe");

        WebDriver webDriver = new ChromeDriver();

        webDriver.get("https://www.bing.com/");
        Assert.assertTrue(true);

        webDriver.findElement(By.id("sb_form_q")).sendKeys("Telerik Academy Alpha");

        webDriver.findElement(By.id("search_icon")).click();

        WebElement firstTitle = webDriver.findElement(By.xpath("//h2[@class=\" b_topTitle\"]//a"));
        Assert.assertEquals("IT Career Start in 6 Months - Telerik Academy Alpha", firstTitle.getText());

        webDriver.quit();
    }


}
