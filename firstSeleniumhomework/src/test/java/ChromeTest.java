import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class ChromeTest {

    @Test
    public void navigateToGoogle() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\user\\Drivers\\chromedriver.exe");


        WebDriver webDriver = new ChromeDriver();
        webDriver.get("https://www.google.com/");

        Assert.assertTrue(true);

        webDriver.findElement(By.id("W0wltc")).click();

        webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

        webDriver.findElement(By.name("q")).sendKeys("Telerik Academy Alpha");

        webDriver.findElement(By.name("btnK")).click();

        WebElement firstTitle = webDriver.findElement(By.xpath("//div[@class=\"yuRUbf\"]//h3"));
        Assert.assertEquals("IT Career Start in 6 Months - Telerik Academy Alpha", firstTitle.getText());

        webDriver.quit();

    }
}
